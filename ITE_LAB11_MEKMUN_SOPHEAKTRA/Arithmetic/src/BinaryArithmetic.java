import java.util.Scanner;

/*
  ITE_LAB 10
  Made By : Mekmun Sopheaktra
  Date: 22/03/2022
  main class
  @since LAB 10.0
 */
public class BinaryArithmetic {

     /** This Method use for sum two binary
     * @param binary1 for first binary
     * @param binary2 for second binary
     * @return binary
     */

    public static String binaryAddition(String binary1,String binary2) {


        StringBuilder sb = new StringBuilder();
        int i = binary1.length() - 1;
        int j = binary2.length() - 1;
        int carry = 0;
        while (i >= 0 || j >= 0) {
            int sum = carry;
            if (j >= 0) {
                sum += binary2.charAt(j--) - '0' ;
            }
            if (i >= 0) {
                sum += binary1.charAt(i--) - '0' ;
            }

            // Added number can be only 0 or 1
            sb.append(sum % 2);

            // Get the carry.
            carry = sum / 2;
        }

        if (carry != 0) { sb.append(carry); }

        // First reverse and then return.
        return sb.reverse().toString();

    }

    /**
     * This Method use for Subtract two binary
     * @param binary1 for first binary
     * @param binary2 for second binary
     * @return Result
     */
    public static String binarySubtraction(String binary1,String binary2){
        StringBuilder result= new StringBuilder();
        String max="";
        char binary='0';

        boolean tf=(binary1.length()>=binary2.length());

        int paramA=binary1.length();
        int paramB=binary2.length();

        if(paramA<paramB) {

            StringBuilder binary1Builder = new StringBuilder(binary1);
            for (int a = 1; a <= paramB - paramA; a++)
                binary1Builder.insert(0, '0');
            binary1 = binary1Builder.toString();

        } else if(paramB<paramA) {

            StringBuilder binary2Builder = new StringBuilder(binary2);
            for (int a = 1; a <= paramA - paramB; a++)
                binary2Builder.insert(0, "0");
            binary2 = binary2Builder.toString();
        }


        if(!tf) {

            for (int a = paramA - 1; a >= 0; a--) {

                if (binary1.charAt(a) != binary2.charAt(a)) {

                    if (binary2.charAt(a) == '1') {

                        max = binary2;
                        binary2 = binary1;
                        binary1 = max;
                        break;

                    }
                }
            }
        }

        for(int a=binary1.length()-1;a>=0;a--){

            if(binary1.charAt(a)=='1' && binary2.charAt(a)=='0'){

                if(binary=='1') {

                    result.insert(0, '0');

                    binary='0';

                }else{

                    result.insert(0, '1');

                }

            }else if(binary1.charAt(a)==binary2.charAt(a) && binary2.charAt(a)=='1'){

                if(binary=='1'){

                    result.insert(0, '1');

                    binary='1';

                }else{
                    result.insert(0, '0');
                }

            }else if(binary1.charAt(a)=='0' && binary2.charAt(a)=='1'){

                if(binary=='1'){

                    result.insert(0, '0');

                }else{

                    result.insert(0, '1');

                        binary = '1';

                }

            } else {

                if(binary=='1'){

                    result.insert(0, '1');

                }else{

                    result.insert(0, '0');

                }
            }
        }

        return result.toString();

    }

    /**
     * This Class use for Binary Complement
     */

static class Complement{ //OuterClass
        private static String binaryOuter1;
        private static String binaryOuter2;

    public Complement(String binary1,String binary2){
        binaryOuter1 = binary1;
        binaryOuter2 = binary2;
    }

        /**
         * this method use for swap 1 to 0 or 0 to 1
         * @param binary binary
         * @return '0' for '1' and '1' for '0'
         */
    static char swap(char binary){
        return (binary == '0') ? '1' : '0'; //return using ternary operator
    }

        /**
         * class use for find firstComplement
         */
    public static class firstComplement{ //InnerClass
        //binary swap here
        public static void display(){
            int Bin1Length = binaryOuter1.length();
            int Bin2Length = binaryOuter2.length();

            StringBuilder ResultStringBin1 = new StringBuilder();
            StringBuilder ResultStringBin2 = new StringBuilder();

            for (int i = 0; i < Bin1Length; i++) {
                ResultStringBin1.append(swap(binaryOuter1.charAt(i)));
            }
            System.out.println("Result of Binary 1 in firstComplement :" +ResultStringBin1);

            for( int j = 0 ;j < Bin2Length; j++){

                ResultStringBin2.append(swap(binaryOuter2.charAt(j)));

            }
            System.out.println("Result of Binary 2 in firstComplement :" +ResultStringBin2);

        }
    }

        /**
         * class use for find secondComplement
         */
    public static class secondComplement { //InnerClass
        public static void display() {
            int BinS1Length = binaryOuter1.length();
            int BinS2Length = binaryOuter2.length();

            StringBuilder ResultStringBin1 = new StringBuilder();
            StringBuilder ResultStringBin2 = new StringBuilder();

            for (int i = 0; i < BinS1Length; i++) {
                ResultStringBin1.append(swap(binaryOuter1.charAt(i)));
            }
            for( int j = 0 ;j < BinS2Length; j++){
                ResultStringBin2.append(swap(binaryOuter2.charAt(j)));
            }

            String  secondBinary1 = binaryAddition(String.valueOf(ResultStringBin1),String.valueOf(1));
            String  secondBinary2 = binaryAddition(String.valueOf(ResultStringBin2),String.valueOf(1));

            System.out.println("Result of Second Complement of Binary 1 :"+secondBinary1);
            System.out.println("Result of Second Complement of Binary 2 :"+secondBinary2);

        }

    }

        /**
         * class use for addition with 2nd Complement
         */
    public static class addWith2ndComplement{ //InnerClass
        public static void displayAdd2ndComplement(){
            int BinS1Length = binaryOuter1.length();
            int BinS2Length = binaryOuter2.length();

            StringBuilder ResultStringBin1 = new StringBuilder();
            StringBuilder ResultStringBin2 = new StringBuilder();

            for (int i = 0; i < BinS1Length; i++) {
                ResultStringBin1.append(swap(binaryOuter1.charAt(i)));
            }
            for( int j = 0 ;j < BinS2Length; j++){
                ResultStringBin2.append(swap(binaryOuter2.charAt(j)));
            }

            String  secondBinary1 = binaryAddition(String.valueOf(ResultStringBin1),String.valueOf(1));
            String  secondBinary2 = binaryAddition(String.valueOf(ResultStringBin2),String.valueOf(1));

            String resultAdd2ndComplement = binaryAddition(secondBinary1,secondBinary2);

            System.out.println("Result of Addition 2nd Complement : "+resultAdd2ndComplement);

            }
        }

        /**
         * class use for substract with 2nd Complement
         */
        public static class substractWith2ndComplement{ //InnerClass
            public static void displaysubstractWith2ndComplement(){
                int BinS1Length = binaryOuter1.length();
                int BinS2Length = binaryOuter2.length();

                StringBuilder ResultStringBin1 = new StringBuilder();
                StringBuilder ResultStringBin2 = new StringBuilder();

                for (int i = 0; i < BinS1Length; i++) {
                    ResultStringBin1.append(swap(binaryOuter1.charAt(i)));
                }
                for( int j = 0 ;j < BinS2Length; j++){
                    ResultStringBin2.append(swap(binaryOuter2.charAt(j)));
                }

                String  secondBinary1 = binaryAddition(String.valueOf(ResultStringBin1),String.valueOf(1));
                String  secondBinary2 = binaryAddition(String.valueOf(ResultStringBin2),String.valueOf(1));

                String resultsubstractWith2ndComplement = binarySubtraction(secondBinary1,secondBinary2);

                System.out.println("Result of Subtraction 2nd Complement : "+resultsubstractWith2ndComplement);
            }
        }
    }

    public static int multiplyBinary(int binary1 , int binary2){
        int i=0,remainder = 0, sum[] = new int[20];
        int binarySum=0;

        while(binary1!=0||binary2!=0){
            sum[i++] =  (binary1 %10 + binary2 %10 + remainder ) % 2;
            remainder = (binary1 %10 + binary2 %10 + remainder ) / 2;
            binary1 = binary1/10;
            binary2 = binary2/10;
        }

        if(remainder!=0)
            sum[i++] = remainder;
        --i;
        while(i>=0)
            binarySum = binarySum*10 + sum[i--];

        return binarySum;

    }
    public static void divideBinary(){

        Scanner scan = new Scanner(System.in);
        System.out.print("Input First Binary : ");

        int firstBinary = Integer.parseInt(scan.nextLine(),2);

        System.out.print("Enter Second Binary : ");

        int secondBinary = Integer.parseInt(scan.nextLine(),2);

            String result = Integer.toBinaryString((firstBinary/secondBinary));

            System.out.println("Result of Binary Divide are : "+result);

            String remainder = Integer.toBinaryString((firstBinary%secondBinary));

            System.out.println("Remainder : "+remainder);
    }

}

