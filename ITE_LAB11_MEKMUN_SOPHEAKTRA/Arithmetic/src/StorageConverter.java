/*
  ITE_LAB7
  Made By : Mekmun Sopheaktra
  Date: 10/02/2022
  StorageConverter class
  @since 7.0
 */
public class StorageConverter {
    /**
     * This Method use for convert Byte to kilobyte
     * @param numByte bye
     * @return ResultOfByte2kb
     */
    public static double byte2KB(long numByte){
        /*
            Covert Byte to Kilobyte : 1 byte = 0.001 kilobyte
         */
        return (double) numByte/0.001;
    }

    /**
     * This Method use for convert Byte to Megabyte
     * @param numByte byte
     * @return ResultOfByte2MB
     */
    public static double byte2MB(long numByte){
        /*
            Covert Byte to Megabyte : 1 byte = 0.000001 Megabyte
         */
        return (double) numByte/0.000001;
    }

    /**
     * This Method use for convert Byte to Gigabyte
     * @param numByte Byte
     * @return ResultOfByte2GB
     */
    public static double byte2GB(long numByte){
        /*
            Covert Byte to Gigabyte : 1 byte = 0.000000001 Gigabyte
         */
        return (double) numByte/0.000000001;
    }

    /**
     * This Method use for convert Byte to Terabyte
     * @param numByte Byte
     * @return ResultOfByte2TB
     */
    public static double byte2TB(long numByte){
        /*
            Covert Byte to Terabyte : 1 byte = 0.000000000001 Terabyte
         */
        return (double) numByte/0.000000000001;
    }

    /**
     * This Method use for convert Byte to Petabyte
     * @param numByte Byte
     * @return ResultOfByte2PB
     */
    public static double byte2PB(long numByte){
        /*
            Covert Byte to Petabyte : 1 byte = 0.000000000000001 Petabyte
         */
        return (double) numByte/0.000000000000001;
    }

    /**
     * This Method use for convert Kilobyte to Byte
     * @param kilobyte Byte
     * @return ResultOfKb2Byte
     */
    public static double kb2Byte(long kilobyte){
        /*
            Covert Kilobyte to Byte : 1 Kilobyte = 1024 Byte
         */
        return (double) kilobyte/1024;
    }

    /**
     * This Method use for convert Kilobyte to Megabyte
     * @param kilobyte kilo
     * @return ResultOfkb2MB
     */
    public static double kb2MB(long kilobyte){
        /*
            Covert Kilobyte to Megabyte : 1 Kilobyte = 0.001 Megabyte
         */
        return (double) kilobyte/0.001;
    }

    /**
     * This Method use for convert Kilobyte to Gigabyte
     * @param kilobyte kilo
     * @return ResultOfkb2GB
     */
    public static double kb2GB(long kilobyte){
        /*
            Covert Kilobyte to Gigabyte : 1 Kilobyte = 0.000001 Gigabyte
         */
        return (double) kilobyte/0.000001;
    }

    /**
     * This Method use for convert Kilobyte to Terabyte
     * @param kilobyte kilo
     * @return Resultofkb2TB
     */
    public static double kb2TB(long kilobyte){
        /*
            Covert Kilobyte to Terabyte : 1 Kilobyte = 0.000000001 Terabyte
         */
        return (double) kilobyte/0.000000001;
    }

    /**
     * This Method use for convert Kilobyte to Petabyte
     * @param kilobyte kilo
     * @return ResultofKb2PB
     */
    public static double kb2PB(long kilobyte){
        /*
            Covert Kilobyte to Petabyte : 1 Kilobyte = 0.000000000001 Petabyte
         */
        return (double) kilobyte/0.000000000001;
    }

    /**
     * This Method use for convert Megabyte to Byte
     * @param megabyte mega
     * @return ResultOfMB2Byte
     */
    public static double mb2Byte(long megabyte){
        /*
            Covert Megabyte to Byte : 1 Megabyte = 1000000 Byte
         */
        return (double) megabyte/1000000;
    }

    /**
     * This Method use for convert Megabyte to KiloByte
     * @param megabyte mega
     * @return ResultOfMb2KB
     */
    public static double mb2KB(long megabyte){
        /*
            Covert Megabyte to KiloByte : 1 Megabyte = 1024 KiloByte
         */
        return (double) megabyte/1024;
    }

    /**
     * This Method use for convert Megabyte to Gigabyte
     * @param megabyte mega
     * @return ResultOfMb2GB
     */
    public static double mb2GB(long megabyte){
        /*
            Covert Megabyte to Gigabyte : 1 Megabyte = 0.001 Gigabyte
         */
        return (double) megabyte/0.001;
    }

    /**
     * This Method use for convert Megabyte to Terabyte
     * @param megabyte mega
     * @return ResultofMB2TB
     */
    public static double mb2TB(long megabyte){
        /*
            Covert Megabyte to Terabyte : 1 Megabyte = 0.000001 Terabyte
         */
        return (double) megabyte/0.000001;
    }

    /**
     * This Method use for convert Megabyte to Petabyte
     * @param megabyte mega
     * @return ResultOfMb2PB
     */
    public static double mb2PB(long megabyte){
        /*
            Covert Megabyte to Petabyte : 1 Megabyte = 0.000000001 Petabyte
         */
        return (double) megabyte/0.000000001;
    }

    /**
     * This Method use for convert Gigabyte to Byte
     * @param gigabyte giga
     * @return ResultofGB2Byte
     */
    public static double gb2Byte(long gigabyte){
        /*
            Covert Gigabyte to Byte : 1 Gigabyte = 1000000000 Byte
         */
        return (double) gigabyte/1000000000;
    }

    /**
     * This Method use for convert Gigabyte to Kilobyte
     * @param gigabyte giga
     * @return ResultOfGB2KB
     */
    public static double gb2KB(long gigabyte){
        /*
            Covert Gigabyte to Kilobyte : 1 Gigabyte = 1000000 Kilobyte
         */
        return (double) gigabyte/1000000;
    }

    /**
     * This Method use for convert Gigabyte to Megabyte
     * @param gigabyte giga
     * @return ResultofGB2MB
     */
    public static double gb2MB(long gigabyte){
        /*
            Covert Gigabyte to Megabyte : 1 Gigabyte = 1024 Megabyte
         */
        return (double) gigabyte/1024;
    }

    /**
     * This Method use for convert Gigabyte to Terabyte
     * @param gigabyte giga
     * @return ResultofGB2TB
     */
    public static double gb2TB(long gigabyte){
        /*
            Covert Gigabyte to Terabyte : 1 Gigabyte = 0.001 Terabyte
         */
        return (double) gigabyte/0.001;
    }

    /**
     * This Method use for convert Gigabyte to Petabyte
     * @param gigabyte giga
     * @return ResultOfgb2PB
     */
    public static double gb2PB(long gigabyte){
        /*
            Covert Gigabyte to Petabyte : 1 Gigabyte = 0.000001 Petabyte
         */
        return (double) gigabyte/0.000001;
    }

    /**
     * This Method use for convert Terabyte to Byte
     * @param terabyte tera
     * @return ResultofTB2Byte
     */
    public static double tb2Byte(long terabyte){
        /*
            Covert Terabyte to Byte : 1 Terabyte = 1000000000000 Byte
         */
        return (double) terabyte/1000000000000L;
    }

    /**
     * This Method use for convert Terabyte to Kilobyte
     * @param terabyte tera
     * @return ResultofTB2KB
     */
    public static double tb2KB(long terabyte){
        /*
            Covert Terabyte to Kilobyte : 1 Terabyte = 1000000000 Kilobyte
         */
        return (double) terabyte/1000000000;
    }

    /**
     * This Method use for convert Terabyte to Megabyte
     * @param terabyte tera
     * @return ResultofTB2MB
     */
    public static double tb2MB(long terabyte){
        /*
            Covert Terabyte to Megabyte : 1 Terabyte = 1000000 Megabyte
         */
        return (double) terabyte/1000000;
    }

    /**
     * This Method use for convert Terabyte to Gigabyte
     * @param terabyte tera
     * @return ResutofTB2GB
     */
    public static double tb2GB(long terabyte){
        /*
            Covert Terabyte to Gigabyte : 1 Terabyte = 1024 Gigabyte
         */
        return (double) terabyte/1024;
    }

    /**
     * This Method use for convert Terabyte to Petabyte
     * @param terabyte tera
     * @return ResultofTB2PB
     */
    public static double tb2PB(long terabyte){
        /*
            Covert Terabyte to Petabyte : 1 Terabyte = 0.001 Petabyte
         */
        return (double) terabyte/0.001;
    }

    /**
     * This Method use for convert Petabyte to Byte
     * @param petabyte peta
     * @return ResultofPB2Byte
     */
    public static double pb2Byte(long petabyte){
        /*
            Covert Petabyte to Byte : 1 Petabyte = 1000000000000000  Byte
         */
        return (double) petabyte/1000000000000000L;
    }

    /**
     * This Method use for convert Petabyte to Kilobyte
     * @param petabyte peta
     * @return ResultofPB2KB
     */
    public static double pb2KB(long petabyte){
        /*
            Covert Petabyte to Kilobyte : 1 Petabyte = 1000000000000000  Kilobyte
         */
        return (double) petabyte/1000000000000L;
    }

    /**
     * This Method use for convert Petabyte to Megabyte
     * @param petabyte peta
     * @return ResultofPB2MB
     */
    public static double pb2MB(long petabyte){
        /*
            Covert Petabyte to Megabyte : 1 Petabyte = 1000000000  Megabyte
         */
        return (double) petabyte/1000000000;
    }

    /**
     * This Method use for convert Petabyte to Gigabyte
     * @param petabyte peta
     * @return ResultofPB2GB
     */
    public static double pb2GB(long petabyte){
        /*
            Covert Petabyte to Gigabyte : 1 Petabyte = 1000000 Gigabyte
         */
        return (double) petabyte/1000000;
    }

    /**
     * This Method use for convert Petabyte to Terabyte
     * @param petabyte peta
     * @return ResultOfPB2TB
     */
    public static double pb2TB(long petabyte){
        /*
            Covert Petabyte to Gigabyte : 1 Terabyte = 1000000 Terabyte
         */
        return (double) petabyte/1000;
    }
}
