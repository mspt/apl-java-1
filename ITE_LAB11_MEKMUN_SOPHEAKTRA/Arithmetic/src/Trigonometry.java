/*
  ITE_LAB7
  Made By : Mekmun Sopheaktra
  Date: 10/02/2022
  Trigonometry class
  @since 7.0
 */
import java.lang.Math;
public class Trigonometry {
    /**
     * This Method use for calculate sin
     * @param radian radian
     * @return SinResult
     */
    public static double sin(double radian){
        return Math.sin(radian);
    }

    /**
     * This Method use for calculate sin in degree
     * @param degree degree
     * @return SindxResult
     */
    public static double sind(double degree){
        double Sindx = Math.toDegrees(degree);
        return Math.sin(Sindx);
    }

    /**
     * This Method use for calculate cos
     * @param radian radian
     * @return CosResult
     */
    public static double cos(double radian){
        return Math.cos(radian);
    }

    /**
     * This Method use for calculate cos in degree
     * @param degree degree
     * @return cosxResult
     */
    public static double cosd(double degree){
        double cosx = Math.toDegrees((degree));
        return Math.cos(cosx);
    }

    /**
     * This Method use for calculate tan
     * @param radian radian
     * @return tanx
     */
    public static double tan(double radian){
        return Math.tan(radian);
    }

    /**
     * This Method use for calculate tan in degree
     * @param degree degree
     * @return tanxResult
     */
    public static double tand(double degree){
        double tandx = Math.toDegrees(degree);
        return Math.tan(tandx);
    }

    /**
     * This Method use for calculate square2
     * @param x x
     * @return sqaure2x
     */
    public static double square2(double x){
        return Math.sqrt(x);
    }
}
