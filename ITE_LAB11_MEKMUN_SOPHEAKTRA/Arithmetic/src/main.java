/*
  ITE_LAB 10
  Made By : Mekmun Sopheaktra
  Date: 22/03/2022
  main class
  @since LAB 10.0
 */
import java.util.Scanner;
//Main Class
public class main {

    public static void main(String[] args) {
        while (true) {
            Scanner scanner = new Scanner(System.in); // use for get value from user
            //Menu Options :
            System.out.println("--------------------------------------------------------------");
            System.out.println("Menu : ");
            System.out.println("1. Arithmetic ");
            System.out.println("2. Bitwise ");
            System.out.println("3. MoneyExchange ");
            System.out.println("4. Trigonometry ");
            System.out.println("5. Storage Converter");
            System.out.println("6. Number System Conversion");
            System.out.println("7. Binary Arithmetic");
            System.out.println("--------------------------------------------------------------");
            //User Input Menu Option :
            System.out.print("Input Numebr of Menu : ");
            int menu = scanner.nextInt();

            switch (menu) {
                case 1 -> {                                                                 //case 1 : Arithmetic
                    System.out.println("Menu 1 : Arithmetic");
                    int number1, number2;
                    System.out.print("Please Input First Number : ");                   //User Input Value for the First Number
                    number1 = scanner.nextInt();
                    System.out.print("Please Input Second Number : ");                  //User Input Value for the Second Number
                    number2 = scanner.nextInt();

                    //Sum Result :
                    int SumResult = Arithmetic.sum(number1, number2);                   // Get value from Method ( Sum ) in Class ( Arithmetic )
                    System.out.println("The Result Of Sum : " + SumResult);             // Output Value

                    //Divide Result :
                    double DivideResult = Arithmetic.divide(number1, number2);          // Get value from Method ( Divide ) in Class ( Arithmetic )
                    System.out.println("The Result Of Divide : " + DivideResult);       // Output Value

                    //Minus Result :
                    int MinusResult = Arithmetic.minus(number1, number2);               // Get value from Method ( Minus ) in Class ( Arithmetic )
                    System.out.println("The Result of Minus : " + MinusResult);         // Output Value

                    //Multiply Result :
                    int MultiplyResult = Arithmetic.multiply(number1, number2);         // Get value from Method ( Multiply ) in Class ( Arithmetic )
                    System.out.println("The Result of Multiply : " + MultiplyResult);   // Output Value


                    //Modulo Result :
                    int ModuloResult = Arithmetic.modulo(number1, number2);             // Get value from Method ( Modulo ) in Class ( Arithmetic )
                    System.out.println("The Result of Modulo : " + ModuloResult);       // Output Value
                }
                //End of Case 1

                case 2 -> {                                                                 //case 2 : Bitwise
                    System.out.println("Menu 2 : Bitwise");
                    int bit1, bit2;
                    System.out.print("Input First Bit : ");                             //user input value of first bit
                    bit1 = scanner.nextInt();
                    System.out.print("Input Second Bit : ");                            //user input value of second bit
                    bit2 = scanner.nextInt();

                    //AND Result :
                    int AndResult = Bitwise.AND(bit1, bit2);                            // Get value from Method ( AND ) in Class ( Bitwise )
                    System.out.println("The Result of AND : " + AndResult);             // Output Value

                    //OR Result :
                    int OrResult = Bitwise.OR(bit1, bit2);                              // Get value from Method ( OR ) in Class ( Bitwise )
                    System.out.println("The Result of OR : " + OrResult);               // Output Value

                    //XOR Result :
                    int XorResult = Bitwise.XOR(bit1, bit2);                            // Get value from Method ( XOR ) in Class ( Bitwise )
                    System.out.println("The Result of XOR : " + XorResult);             // Output Value

                    //LeftShift Result :
                    int LeftShiftResult = Bitwise.leftShift(bit1, bit2);                // Get value from Method ( leftShift ) in Class ( Bitwise )
                    System.out.println("Thr Result of LeftShift : " + LeftShiftResult); // Output Value

                    //RightShift Result :
                    int RightShiftResult = Bitwise.rightShift(bit1, bit2);              // Get value from Method ( rightShift ) in Class ( Bitwise )
                    System.out.println("The Result of RightShift : " + RightShiftResult);// Output Value

                    //Inversion Result :
                    int InversionResult = Bitwise.bitInversion(bit1);                   // Get value from Method ( bitInversion ) in Class ( Bitwise )
                    System.out.println("The Result of Inversion : " + InversionResult); // Output Value
                }
                //End of Case 2
                case 3 -> {                                                                 //case 3 : Moneys Exchange
                    System.out.println("Menu 3 : MoneyExchange");
                    int MoneyMenu;
                    System.out.println("--------------------------------------------------------------");
                    System.out.println("1.Money Exchange Riel to USD , Euro , Frank , Pound , Baht");
                    System.out.println("2.Money Exchange USD to Riel");
                    System.out.println("3.Money Exchange Euro to Riel ");
                    System.out.println("3.Money Exchange Frank to Riel ");
                    System.out.println("3.Money Exchange Pound to Riel ");
                    System.out.println("3.Money Exchange Baht to Riel ");
                    System.out.println("--------------------------------------------------------------");
                    System.out.print("Input Menu : ");
                    MoneyMenu = scanner.nextInt();                                      //user input Menu Option for Money Exchange
                    switch (MoneyMenu) {
                        case 1 -> {                                                         //case 1 : Money Exchange Riel to USD , Euro , Frank , Pound , Baht
                            System.out.println("Money Exchange Riel to USD , Euro , Frank , Pound , Baht");
                            System.out.print("Input in Riel : ");
                            double riel = scanner.nextDouble();                         //user input Value in Riel

                            //Riel to USD
                            double RieltoUSD = MoneyExchange.RieltoUSD(riel);           // Get value from Method ( RieltoUSD ) in Class ( MoneyExchange )
                            System.out.println("Result in USD : " + RieltoUSD);         // Output Value

                            //Riel to Euro
                            double RieltoEuro = MoneyExchange.RieltoEuro(riel);         // Get value from Method ( RieltoEuro ) in Class ( MoneyExchange )
                            System.out.println("Result in Euro : " + RieltoEuro);       // Output Value

                            //Riel to Frank
                            double RieltoFrank = MoneyExchange.RieltoFrank(riel);       // Get value from Method ( RieltoFrank ) in Class ( MoneyExchange )
                            System.out.println("Result in Frank : " + RieltoFrank);     // Output Value

                            //Riel to Frank
                            double RieltoPound = MoneyExchange.RieltoPound(riel);       // Get value from Method ( RieltoPound ) in Class ( MoneyExchange )
                            System.out.println("Result in Pound : " + RieltoPound);     // Output Value

                            //Riel to Baht
                            double RieltoBaht = MoneyExchange.RieltoBaht(riel);         // Get value from Method ( RieltoPound ) in Class ( MoneyExchange )
                            System.out.println("Result in Baht : " + RieltoBaht);       // Output Value
                        }
                        case 2 -> {                                                         //case 2 : Money Exchange USD to Riel
                            //USD to Riel
                            System.out.println("Money Exchange USD to Riel");
                            System.out.print("Input in USD : ");
                            double usd = scanner.nextDouble();                          //user input Value in USD
                            double USDtoRiel = MoneyExchange.USDtoRiel(usd);            // Get value from Method ( USDtoRiel ) in Class ( MoneyExchange )
                            System.out.println("Result in Riel : " + USDtoRiel);        // Output Value
                        }
                        case 3 -> {
                            //Euro to Riel
                            System.out.println("Money Exchange Euro to Riel");
                            System.out.print("Input in Euro : ");
                            double euro = scanner.nextDouble();                         //user input Value in Euro
                            double EurotoRiel = MoneyExchange.EurotoRiel(euro);         // Get value from Method ( EurotoRiel ) in Class ( MoneyExchange )
                            System.out.println("Result in Riel : " + EurotoRiel);       // Output Value
                        }
                        case 4 -> {
                            //Frank to Riel
                            System.out.println("Money Exchange Frank to Riel");
                            System.out.print("Input in Frank : ");
                            double Frank = scanner.nextDouble();                        //user input Value in Frank
                            double FranktoRiel = MoneyExchange.FranktoRiel(Frank);      // Get value from Method ( FranktoRiel ) in Class ( MoneyExchange )
                            System.out.println("Result in Riel : " + FranktoRiel);      // Output Value
                        }
                        case 5 -> {
                            //Pound to Riel
                            System.out.println("Money Exchange Pound to Riel");
                            System.out.print("Input in Pound : ");
                            double Pound = scanner.nextDouble();                        //user input Value in Pound
                            double PoundtoRiel = MoneyExchange.PoundtoRiel(Pound);      // Get value from Method ( PoundtoRiel ) in Class ( MoneyExchange )
                            System.out.println("Result in Riel : " + PoundtoRiel);      // Output Value
                        }
                        case 6 -> {
                            //Baht to Riel
                            System.out.println("Money Exchange Baht to Riel");
                            System.out.print("Input in Baht : ");
                            double Baht = scanner.nextDouble();                         //user input Value in Baht
                            double BahttoRiel = MoneyExchange.BahttoRiel(Baht);         // Get value from Method ( BahttoRiel ) in Class ( MoneyExchange )
                            System.out.println("Result in Riel : " + BahttoRiel);       // Output Value
                        }
                    }
                }
                case 4 -> {                                                                    //case 4 : Trigonometry
                    System.out.println("Menu 4 : Trigonometry");
                    System.out.print("Input Value : ");
                    double value = scanner.nextDouble();                                   //user input Value

                    //sin
                    double Resultofsin = Trigonometry.sin(value);                          // Get value from Method ( sin ) in Class ( Trigonometry )
                    System.out.println("Result of Sin : " + Resultofsin);                  // Output Value

                    //sin in degree
                    double Resultofsind = Trigonometry.sind(value);                         // Get value from Method ( sind ) in Class ( TTrigonometry )
                    System.out.println("Result of Sin in Degree : " + Resultofsind);        // Output Value

                    //cos
                    double Resultofcos = Trigonometry.cos(value);                           // Get value from Method ( cos ) in Class ( MTrigonometry )
                    System.out.println("Result of Cos : " + Resultofcos);                   // Output Value

                    //cos in degree
                    double Resultofcosd = Trigonometry.cosd(value);                         // Get value from Method ( cosd ) in Class ( Trigonometry )
                    System.out.println("Result of Cos in Degree : " + Resultofcosd);        // Output Value

                    //tan
                    double Resultoftan = Trigonometry.tan(value);                           // Get value from Method ( tan ) in Class ( Trigonometry )
                    System.out.println("Result of Tan : " + Resultoftan);                   // Output Value

                    //tan in degree
                    double Resultoftand = Trigonometry.tand(value);                         // Get value from Method ( tand ) in Class ( Trigonometry )
                    System.out.println("Result of Tan in Degree : " + Resultoftand);        // Output Value

                    //Square 2
                    double Resultofsquare2 = Trigonometry.square2(value);                   // Get value from Method ( square2 ) in Class ( Trigonometry )
                    System.out.println("Result of Square 2 : " + Resultofsquare2);          // Output Value
                }
                case 5 -> {                                                                    //case 5 : Storage Converter
                    int ChooseMenu;
                    System.out.println("-----------------------------------------------");
                    System.out.println("Menu 5 : Storage Converter");
                    System.out.println("Menu : ");
                    System.out.println("1.Byte");
                    System.out.println("2.Kilobyte");
                    System.out.println("3.Megabyte");
                    System.out.println("4.Gigabyte");
                    System.out.println("5.Terabyte");
                    System.out.println("6.Petabyte");
                    System.out.print("Please Input Number Menu : ");
                    ChooseMenu = scanner.nextInt();                                         //user input menu choose
                    switch (ChooseMenu) {
                        case 1 -> {
                            long ValueOfByte;
                            System.out.println("----------------------------------------");
                            System.out.println("1.Byte : ");
                            System.out.print("Please Input : ");
                            ValueOfByte = scanner.nextLong();                               //user input value in Byte

                            //Start of Convert Byte to Other Size
                            // Byte to Kilobyte
                            double ResultofByte2KB = StorageConverter.byte2KB(ValueOfByte); // Get value from Method ( byte2KB ) in Class ( StorageConverter )
                            System.out.println("Result of Byte to KB : " + ResultofByte2KB);  // Output Value

                            // Byte to Megabyte
                            double ResultofByte2MB = StorageConverter.byte2MB(ValueOfByte); // Get value from Method ( byte2MB ) in Class ( StorageConverter )
                            System.out.println("Result of Byte to MB : " + ResultofByte2MB);  // Output Value

                            // Byte to Gigabyte
                            double ResultofByte2GB = StorageConverter.byte2GB(ValueOfByte); // Get value from Method ( byte2GB ) in Class ( StorageConverter )
                            System.out.println("Result of Byte to GB : " + ResultofByte2GB);  // Output Value

                            // Byte to Terabyte
                            double ResultofByte2TB = StorageConverter.byte2TB(ValueOfByte); // Get value from Method ( byte2TB ) in Class ( StorageConverter )
                            System.out.println("Result of Byte to TB : " + ResultofByte2TB);  // Output Value

                            // Byte to Petabyte
                            double ResultofByte2PB = StorageConverter.byte2PB(ValueOfByte); // Get value from Method ( byte2PB ) in Class ( StorageConverter )
                            System.out.println("Result of Byte to PB : " + ResultofByte2PB);  // Output Value
                        }
                        //End of Convert Byte to Other Size
                        case 2 -> {
                            long ValueOfKilobyte;
                            System.out.println("----------------------------------------");
                            System.out.println("1.Kilobyte : ");
                            System.out.print("Please Input : ");
                            ValueOfKilobyte = scanner.nextLong();                                       //user input value in Kilobyte

                            //Start of Convert Kilobyte to other size
                            double Resultofkb2yte = StorageConverter.kb2Byte(ValueOfKilobyte);          // Get value from Method ( kb2Byte ) in Class ( StorageConverter )
                            System.out.println("Result of Kilobyte to Byte : " + Resultofkb2yte);         // Output Value
                            double ResultofKB2MB = StorageConverter.kb2MB(ValueOfKilobyte);             // Get value from Method ( kb2MB ) in Class ( StorageConverter )
                            System.out.println("Result of Kilobyte to Megabyte : " + ResultofKB2MB);      // Output Value
                            double ResultofKB2GB = StorageConverter.kb2GB(ValueOfKilobyte);             // Get value from Method ( kb2GB ) in Class ( StorageConverter )
                            System.out.println("Result of Kilobyte to Gigabyte : " + ResultofKB2GB);      // Output Value
                            double ResultofKB2TB = StorageConverter.kb2TB(ValueOfKilobyte);             // Get value from Method ( kb2TB ) in Class ( StorageConverter )
                            System.out.println("Result of Kilobyte to Terabyte : " + ResultofKB2TB);      // Output Value
                            double Resultofkb2pb = StorageConverter.kb2PB(ValueOfKilobyte);             // Get value from Method ( kb2PB ) in Class ( StorageConverter )
                            System.out.println("Result of Kilobyte to Petabyte : " + Resultofkb2pb);      // Output Value
                        }
                        //End of Convert Kilobyte to other size
                        case 3 -> {
                            long ValueOfMegabyte;
                            System.out.println("----------------------------------------");
                            System.out.println("3.Megabyte : ");
                            System.out.print("Please Input : ");
                            ValueOfMegabyte = scanner.nextLong();                                       //user input value in Megabyte

                            //Start of Convert Megabyte to other size
                            double Resultofmb2yte = StorageConverter.mb2Byte(ValueOfMegabyte);          // Get value from Method ( mb2Byte) in Class ( StorageConverter )
                            System.out.println("Result of Megabyte to Byte : " + Resultofmb2yte);         // Output Value

                            double ResultofMB2KB = StorageConverter.mb2KB(ValueOfMegabyte);             // Get value from Method ( mb2KB ) in Class ( StorageConverter )
                            System.out.println("Result of Megabyte to Kilobyte : " + ResultofMB2KB);      // Output Value

                            double ResultofMB2GB = StorageConverter.mb2GB(ValueOfMegabyte);             // Get value from Method ( mb2GB ) in Class ( StorageConverter )
                            System.out.println("Result of Megabyte to Gigabyte : " + ResultofMB2GB);      // Output Value

                            double ResultofMB2TB = StorageConverter.mb2TB(ValueOfMegabyte);             // Get value from Method ( mb2TB ) in Class ( StorageConverter )
                            System.out.println("Result of Megabyte to Terabyte : " + ResultofMB2TB);      // Output Value

                            double ResultofMB2pb = StorageConverter.mb2PB(ValueOfMegabyte);             // Get value from Method ( mb2PB ) in Class ( StorageConverter )
                            System.out.println("Result of Megabyte to Petabyte : " + ResultofMB2pb);      // Output Value
                        }
                        //End of Convert Megabyte to other size
                        case 4 -> {
                            long ValueOfGigabyte;
                            System.out.println("----------------------------------------");
                            System.out.println("1.Gigabyte: ");
                            System.out.print("Please Input : ");
                            ValueOfGigabyte = scanner.nextLong();                                       //user input value in Megabyte

                            //Start of Convert Gigabyte to other size
                            double ResultofGB2yte = StorageConverter.gb2Byte(ValueOfGigabyte);          // Get value from Method ( gb2Byte ) in Class ( StorageConverter )
                            System.out.println("Result of Gigabyte to Byte : " + ResultofGB2yte);         // Output Value

                            double ResultofGB2MB = StorageConverter.gb2KB(ValueOfGigabyte);             // Get value from Method ( gb2KB ) in Class ( StorageConverter )
                            System.out.println("Result of Gigabyte to Kilobyte : " + ResultofGB2MB);      // Output Value

                            double Resultofgb2MB = StorageConverter.gb2MB(ValueOfGigabyte);             // Get value from Method ( gb2MB ) in Class ( StorageConverter )
                            System.out.println("Result of Gigabyte to Megabyte : " + Resultofgb2MB);      // Output Value

                            double ResultofGB2TB = StorageConverter.gb2TB(ValueOfGigabyte);             // Get value from Method ( gb2MB ) in Class ( StorageConverter )
                            System.out.println("Result of Gigabyte to Terabyte : " + ResultofGB2TB);      // Output Value

                            double ResultofGB2pb = StorageConverter.gb2PB(ValueOfGigabyte);             // Get value from Method ( gb2PB ) in Class ( StorageConverter )
                            System.out.println("Result of Gigabyte to Petabyte : " + ResultofGB2pb);      // Output Value
                        }
                        //End of Convert Gigabyte to other size
                        case 5 -> {
                            long ValueOfTerabyte;
                            System.out.println("----------------------------------------");
                            System.out.println("1.Terabyte: ");
                            System.out.print("Please Input : ");
                            ValueOfTerabyte = scanner.nextLong();                                       //user input value in Terabyte

                            //Start of Convert Terabyte to other size
                            double ResultofTB2yte = StorageConverter.tb2Byte(ValueOfTerabyte);          // Get value from Method ( tb2Byte ) in Class ( StorageConverter )
                            System.out.println("Result of Terabyte to Byte : " + ResultofTB2yte);         // Output Value

                            double ResultofTB2KB = StorageConverter.tb2KB(ValueOfTerabyte);             // Get value from Method ( tb2KB ) in Class ( StorageConverter )
                            System.out.println("Result of Terabyte to Kilobyte : " + ResultofTB2KB);      // Output Value

                            double Resultoftb2MB = StorageConverter.tb2MB(ValueOfTerabyte);             // Get value from Method ( tb2MB ) in Class ( StorageConverter )
                            System.out.println("Result of Terabyte to Megabyte : " + Resultoftb2MB);      // Output Value

                            double Resultoftb2GB = StorageConverter.tb2GB(ValueOfTerabyte);             // Get value from Method ( tb2GB ) in Class ( StorageConverter )
                            System.out.println("Result of Terabyte to Gigabyte : " + Resultoftb2GB);      // Output Value

                            double Resultoftb2pb = StorageConverter.tb2PB(ValueOfTerabyte);             // Get value from Method ( tb2PB ) in Class ( StorageConverter )
                            System.out.println("Result of Terabyte to Petabyte : " + Resultoftb2pb);      // Output Value
                        }
                        //End of Convert Terabyte to other size
                        case 6 -> {
                            long ValueOfPetabyte;
                            System.out.println("----------------------------------------");
                            System.out.println("1.Petabyte: ");
                            System.out.print("Please Input : ");
                            ValueOfPetabyte = scanner.nextLong();                                       //user input value in Terabyte

                            //Start of Convert Petabyte to other size
                            double Resultofpb2yte = StorageConverter.pb2Byte(ValueOfPetabyte);          // Get value from Method ( pb2Byte ) in Class ( StorageConverter )
                            System.out.println("Result of Petabyte to Byte : " + Resultofpb2yte);         // Output Value

                            double ResultofPB2KB = StorageConverter.pb2KB(ValueOfPetabyte);             // Get value from Method ( pb2KB ) in Class ( StorageConverter )
                            System.out.println("Result of Petabyte to Kilobyte : " + ResultofPB2KB);      // Output Value

                            double Resultofpb2MB = StorageConverter.pb2MB(ValueOfPetabyte);             // Get value from Method ( pb2MB ) in Class ( StorageConverter )
                            System.out.println("Result of Petabyte to Megabyte : " + Resultofpb2MB);      // Output Value

                            double Resultofpb2GB = StorageConverter.pb2GB(ValueOfPetabyte);             // Get value from Method ( pb2GB ) in Class ( StorageConverter )
                            System.out.println("Result of Petabyte to Gigabyte : " + Resultofpb2GB);      // Output Value

                            double Resultofpb2tb = StorageConverter.pb2TB(ValueOfPetabyte);             // Get value from Method ( pb2TB ) in Class ( StorageConverter )
                            System.out.println("Result of Petabyte to Terabyte : " + Resultofpb2tb);      // Output Value
                        }
                        //End of Convert Petabyte to other size
                    }
                }
                case 6 -> {
                    System.out.println("----------------------------------------");
                    System.out.println("6. Number System Conversion ");
                    int menusystem;
                    System.out.println("----------------------------------------");
                    System.out.println("1. Binary to other");
                    System.out.println("2. Octal to other");
                    System.out.println("3. Decimal to other");
                    System.out.println("4. Hexadecimal to other");
                    System.out.print("Input Choose : ");
                    menusystem = scanner.nextInt();                                                                 //user input Menu Choose
                    System.out.println("----------------------------------------");
                    switch (menusystem) {
                        case 1 -> {
                            long numbersystem;
                            System.out.print("Input Value : ");
                            numbersystem = scanner.nextLong();
                            long ResultBinary2Decimal = NumberSystemConversion.binary2Decimal(numbersystem);        // Get value from Method ( binary2Decimal ) in Class ( NumberSystemConversion )
                            System.out.println("Result of Binary to Decimal : " + ResultBinary2Decimal);            // Output Value
                            long ResultBinary2Octal = NumberSystemConversion.binary2Octal(numbersystem);            // Get value from Method ( binary2Octal ) in Class ( NumberSystemConversion )
                            System.out.println("Result of Binary to Octal : " + ResultBinary2Octal);                // Output Value
                            long ResultBinary2Hex = NumberSystemConversion.octal2Hex(numbersystem);                 // Get value from Method ( binary2Hex ) in Class ( NumberSystemConversion )
                            System.out.println("Result of Binary to Hex : " + ResultBinary2Hex);                    // Output Value
                        }
                        case 2 -> {
                            long numbersystem1;
                            System.out.print("Input Value : ");
                            numbersystem1 = scanner.nextLong();
                            long resultOctal2Binary = NumberSystemConversion.octal2Binary(numbersystem1);            // Get value from Method ( octal2Binary ) in Class ( NumberSystemConversion )
                            System.out.println("Result of Octal to Binary : " + resultOctal2Binary);                // Output Value
                            long ResultOctal2Decimal = NumberSystemConversion.octal2Decimal(numbersystem1);          // Get value from Method ( octal2Decimal ) in Class ( NumberSystemConversion )
                            System.out.println("Result of Octal to Decimal : " + ResultOctal2Decimal);              // Output Value
                            long ResultOctal2hex = NumberSystemConversion.octal2Hex(numbersystem1);                  // Get value from Method ( octal2Hex ) in Class ( NumberSystemConversion )
                            System.out.println("Result of Octal to hex : " + ResultOctal2hex);                      // Output Value
                        }
                        case 3 -> {
                            long numbersystem2;
                            System.out.print("Input Value : ");
                            numbersystem2 = scanner.nextLong();
                            long Resultdecimal2Binary = NumberSystemConversion.decimal2Binary(numbersystem2);        // Get value from Method ( decimal2Binary ) in Class ( NumberSystemConversion )
                            System.out.println("Result of Decimal to Binary : " + Resultdecimal2Binary);              // Output Value
                            long Resultdecimal2Octal = NumberSystemConversion.binary2Octal(numbersystem2);           // Get value from Method ( decimal2Octal ) in Class ( NumberSystemConversion )
                            System.out.println("Result of Decimal to Octal : " + Resultdecimal2Octal);                // Output Value
                            String Resultdecimal2Hex = NumberSystemConversion.decimal2Hex(numbersystem2);            // Get value from Method ( decimal2Hex ) in Class ( NumberSystemConversion )
                            System.out.println("Result of Decimal to Hex : " + Resultdecimal2Hex);                    // Output Value
                        }
                        case 4 -> {
                            int menu3;
                            String numbersystem3, numbersystem4, numbersystem5;
                            System.out.println("----------------------------------------");
                            System.out.println("1. Hex to Binary");
                            System.out.println("2. Hex to Octal");
                            System.out.println("3. Hex to Decimal");
                            System.out.print("Input Choose : ");
                            menu3 = scanner.nextInt();                                                                 //user input Menu Choose
                            System.out.println("----------------------------------------");
                            switch (menu3) {
                                case 1 -> {
                                    System.out.print("Input Value : ");
                                    scanner.nextLine();                                                                //User input Value
                                    numbersystem3 = scanner.nextLine();
                                    char hexdec[];
                                    hexdec = numbersystem3.toCharArray();

                                    // Convert HexaDecimal to Binary
                                    System.out.print("\nResult of Hex to Binary : ");
                                    try {
                                        NumberSystemConversion.hex2Binary(hexdec);
                                    } catch (ArrayIndexOutOfBoundsException e) {
                                        System.out.print("");
                                    }
                                    System.out.println("\n");
                                }
                                case 2 -> {
                                    System.out.print("Input Value : ");
                                    scanner.nextLine();
                                    numbersystem4 = scanner.nextLine();
                                    NumberSystemConversion.hex2Octal((numbersystem4));
                                }
                                case 3 -> {
                                    System.out.print("Input Value : ");
                                    scanner.nextLine();
                                    numbersystem5 = scanner.nextLine();
                                    NumberSystemConversion.hex2Decimal(numbersystem5);
                                    long ResultHex2Decimal = NumberSystemConversion.hex2Decimal(numbersystem5);
                                    System.out.println("Result of Hex to Decimal : " + ResultHex2Decimal);
                                }
                            }
                        }
                    }
                }
                case 7 -> {
                    int menu4;
                    System.out.println("----------------------------------------");
                    System.out.println("Menu 7 - Binary Arithmetic : ");
                    System.out.println("1. Addition Binary");
                    System.out.println("2. Subtraction Binary");
                    System.out.println("3. FirstComplement");
                    System.out.println("4. SecondComplement");
                    System.out.println("5. Addition 2 Binary of SecondComplement");
                    System.out.println("6. Subtraction 2 Binary of SecondComplement");
                    System.out.println("7. Multiply Binary");
                    System.out.println("8. Divide Binary");
                    System.out.print("Please Choose : ");
                    menu4 = scanner.nextInt();
                    switch (menu4) {
                        case 1 -> {
                            //Two variables to hold two input binary numbers
                            String binaryAdd1, binaryAdd2;
                            System.out.println();
                            System.out.print("Enter first binary number: ");
                            binaryAdd1 = scanner.next();

                            //getting second binary number from user
                            System.out.print("Enter second binary number: ");
                            binaryAdd2 = scanner.next();
                            String ResultAdd;
                            ResultAdd = BinaryArithmetic.binaryAddition(binaryAdd1, binaryAdd2);
                            System.out.println("Result of Binary Addition : " + ResultAdd);
                        }
                        case 2 -> {
                            //Two variables to hold two input binary numbers
                            String binarySub1, binarySub2;
                            System.out.println();
                            System.out.print("Enter first binary number: ");
                            binarySub1 = scanner.next();

                            //getting second binary number from user
                            System.out.print("Enter second binary number: ");
                            binarySub2 = scanner.next();
                            String ResultSub;
                            ResultSub = BinaryArithmetic.binarySubtraction(binarySub1, binarySub2);
                            System.out.println("Result of Binary Subtraction : " + ResultSub);
                        }
                        case 3 -> {
                            //Two variables to hold two input binary numbers
                            String binaryF1, binaryF2;
                            System.out.println();
                            System.out.print("Enter first binary number: ");
                            binaryF1 = scanner.next();

                            //getting second binary number from user
                            System.out.print("Enter second binary number: ");
                            binaryF2 = scanner.next();
                            BinaryArithmetic.Complement complement = new BinaryArithmetic.Complement(binaryF1, binaryF2);
                            BinaryArithmetic.Complement.firstComplement.display();
                        }
                        case 4 -> {
                            String binaryS1, binaryS2;
                            System.out.println();
                            System.out.print("Enter first binary number: ");
                            binaryS1 = scanner.next();

                            //getting second binary number from user
                            System.out.print("Enter second binary number: ");
                            binaryS2 = scanner.next();
                            BinaryArithmetic.Complement complement1 = new BinaryArithmetic.Complement(binaryS1, binaryS2);
                            BinaryArithmetic.Complement.secondComplement.display();
                        }
                        case 5 -> {
                            String binaryAdd1Second, binaryAdd2Second;
                            System.out.println();
                            System.out.print("Enter first binary number: ");
                            binaryAdd1Second = scanner.next();

                            //getting second binary number from user
                            System.out.print("Enter second binary number: ");
                            binaryAdd2Second = scanner.next();
                            BinaryArithmetic.Complement complementadd2nd = new BinaryArithmetic.Complement(binaryAdd1Second, binaryAdd2Second);
                            BinaryArithmetic.Complement.addWith2ndComplement.displayAdd2ndComplement();
                        }
                        case 6 -> {
                            System.out.println();
                            System.out.print("Enter first binary number: ");
                            String binarySun1Second = scanner.next();

                            //getting second binary number from user
                            System.out.print("Enter second binary number: ");
                            String binarySun2Second = scanner.next();
                            BinaryArithmetic.Complement complementsub2nd = new BinaryArithmetic.Complement(binarySun1Second, binarySun2Second);
                            BinaryArithmetic.Complement.substractWith2ndComplement.displaysubstractWith2ndComplement();
                        }
                        case 7 -> {

                            int binary1;
                            int binary2;

                            int multiply = 0;
                            int digit = 0, factor = 1;
                            System.out.println();
                            System.out.print("Enter first binary number: ");
                            binary1 = scanner.nextInt();

                            //getting second binary number from user
                            System.out.print("Enter second binary number: ");
                            binary2 = scanner.nextInt();

                            if (digit == 1) {
                                binary1 = binary1 * factor;
                                multiply = Integer.parseInt(BinaryArithmetic.binaryAddition(String.valueOf(binary1), String.valueOf(multiply)));
                            } else
                                binary1 = binary1 * factor;

                            binary2 = binary2 / 10;
                            factor = 10;
                            System.out.println("Result of Multiply Binary : " + BinaryArithmetic.multiplyBinary(binary1, binary2));
                        }

                        case 8 -> {
                            System.out.println();
                            BinaryArithmetic.divideBinary();
                        }


                        default -> {
                            System.out.println("You Input Wrong Number !!"); //if user input wrong number
                            System.out.println(System.lineSeparator().repeat(100));// clear console
                        }

                    }
                }
            }
        }
    }
}

