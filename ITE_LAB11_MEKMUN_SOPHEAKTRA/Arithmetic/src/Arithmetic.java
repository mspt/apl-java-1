/*
  ITE_LAB7
  Made By : Mekmun Sopheaktra
  Date: 10/02/2022
  Arithmetic class
  @since 7.0
 */
public class Arithmetic {
    /**
     * This Method use for sum two value
     * @param a is a first variable
     * @param b is a second variable
     * @return Addition
     */
    public static int sum(int a, int b){
        //Addition operation
        return a + b;
    }

    /**
     * This Method use for divide two value
     * @param a is a first variable
     * @param b is a second variable
     * @return divide
     */
    public static double divide(int a, int b){
        //Dividing operation
        return a / b;
    }

    /**
     * This Method use for minus two value
     * @param a is a first variable
     * @param b is a second variable
     * @return Subtraction
     */
    public static int minus(int a, int b){
        //Subtraction operation
        return a - b;
    }

    /**
     * This Method use for multiply two value
     * @param a is a first variable
     * @param b is a second variable
     * @return Multiplication
     */
    public static int multiply(int a, int b){
        //Multiplication operation
        return a * b;
    }

    /**
     * This Method use for modulo two value
     * @param a is a first variable
     * @param b is a second variable
     * @return Modulus
     */
    public static int modulo(int a, int b){
        //Modulus operation
        return a % b;
    }
}
