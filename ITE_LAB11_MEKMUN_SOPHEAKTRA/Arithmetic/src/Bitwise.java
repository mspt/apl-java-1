/*
  ITE_LAB7
  Made By : Mekmun Sopheaktra
  Date: 10/02/2022
  Bitwise class
  @since 7.0
 */
public class Bitwise {
    /**
     * This Method use for calculate AND Gate
     * @param a first
     * @param b second
     * @return AndRsult
     */
    public static int AND(int a,int b){
        return a&b;
    }

    /**
     * This Method use for calculate OR Gate
     * @param a first
     * @param b second
     * @return OrResult
     */
    public static int OR(int a,int b){
        return a|b;
    }

    /**
     * This Method use for calculate XOR Gate
     * @param a first
     * @param b second
     * @return XorResult
     */
    public static int XOR(int a,int b){
        return a ^ b;
    }
    public static int leftShift(int a,int b){
        return a << b;
    }
    public static int rightShift(int a,int b){
        return a >> b;
    }
    public static int bitInversion(int a){
        return ~a;
    }
}
