/*
  ITE_LAB7
  Made By : Mekmun Sopheaktra
  Date: 10/02/2022
  MoneyExchange class
  @since 7.0
 */
public class MoneyExchange {
    /**
     * This Method use for convert Riel to USD
     * @param Riel use input in riel
     * @return Result
     */
    public static double RieltoUSD(double Riel){
        return Riel/4000;
    }

    /**
     * This Method use for convert Riel to Euro
     * @param Riel user input in riel
     * @return Result
     */
    public static double RieltoEuro(double Riel){
        return Riel/4628.35;
    }

    /**
     * This Method use for convert Riel to Frank
     * @param Riel user input in riel
     * @return Result
     */
    public static double RieltoFrank(double Riel){
        return Riel/4449.87;
    }

    /**
     * This Method use for convert Riel to Pound
     * @param Riel user input in riel
     * @return Result
     */
    public static double RieltoPound(double Riel){
        return Riel/5534.54;
    }

    /**
     * This Method use for convert Riel to Baht
     * @param Riel user input in riel
     * @return Result
     */
    public static double RieltoBaht(double Riel){
        return Riel/123.05;
    }

    /**
     * This Method use for convert USD to Riel
     * @param USD user input in USD
     * @return Result
     */
    public static double USDtoRiel(double USD){
        return USD*4000;
    }

    /**
     * This Method use for convert Euro to Riel
     * @param Euro user input in Euro
     * @return Result
     */
    public static double EurotoRiel(double Euro){
        return Euro*4628.35;
    }

    /**
     * This Method use for convert Frank to Riel
     * @param Frank user input in frank
     * @return Result
     */
    public static double FranktoRiel(double Frank){
        return Frank*4449.87;
    }

    /**
     * This Method use for convert Pound to Riel
     * @param Pound user input in pound
     * @return Result
     */
    public static double PoundtoRiel(double Pound){
        return Pound*5534.54;
    }

    /**
     * This Method use for convert Baht to Riel
     * @param Baht user input in baht
     * @return Result
     */
    public static double BahttoRiel(double Baht){
        return Baht*123.05;
    }
}
