/*
  ITE_LAB7
  Made By : Mekmun Sopheaktra
  Date: 10/02/2022
  NumberSystemConversion class
  @since 7.0
 */
public class NumberSystemConversion {
    /**
     * This Method use for convert Binary to Decimal
     * @param binary binary
     * @return decimal
     */
    public static long binary2Decimal(long binary) {
        /*
        Method make for convert binary to decimal
         */
        long decimal = 0; //Declaring variable to store decimal number
        long n = 0; //Declaring variable to use in power
        while (true) {
            if (binary == 0) { // if binary = 0 terminate loops
                break;
            } else { //if binary not 0
                long temp = binary % 10; // binary divide with 10 to find Remaining number
                decimal += temp * Math.pow(2, n);
                binary = binary / 10;
                n++;
            }
        }
        return decimal; //return value in decimal
    }

    /**
     * This Method use for convert Binary to Octal
     * @param binary binary
     * @return octal
     */
    public static long binary2Octal(long binary) {
        int octal = 0;
        int decimal = 0, i = 0;

        while (binary != 0) {
            decimal += (binary % 10) * Math.pow(2, i);
            ++i;
            binary /= 10;
        }
        i = 1;
        while (decimal != 0) {
            octal += (decimal % 8) * i;
            decimal /= 8;
            i *= 10;
        }

        return octal;
    }

    /**
     * This Method use for convert Binary to Hexadecimal
     * @param binary binary
     * @return Hex
     */
    public static String binary2Hex(long binary) {
        int c = 0;
        long num1 = 0;
        StringBuilder Hex = new StringBuilder();
        while (binary > 0) {
            num1 = num1 + (long) (Math.pow(2, c) * (binary % 10));
            binary = binary / 10;
            c++;
        }
        while (num1 > 0) {
            long j = num1 % 16;
            if (j == 10) {
                Hex.insert(0, "A");
            } else if (j == 11) {
                Hex.insert(0, "B");
            } else if (j == 12) {
                Hex.insert(0, "C");
            } else if (j == 13) {
                Hex.insert(0, "D");
            } else if (j == 14) {
                Hex.insert(0, "E");
            } else if (j == 15) {
                Hex.insert(0, "F");
            } else {
                Hex.insert(0, j);
            }
            num1 = num1 / 16;
        }
        return Hex.toString();
    }

    /**
     * This Method use for convert Octal to Decimal
     * @param octal octal
     * @return decimal
     */
    public static long octal2Decimal(long octal) {

        int decimal = 0; //Declaring variable to store decimal number

        int n = 0; //Declaring variable to use in power
        //writing logic
        while (true) {
            if (octal == 0) {
                break;
            } else {
                long temp = octal % 10;
                decimal += temp * Math.pow(8, n);
                octal = octal / 10;
                n++;
            }
        }
        return decimal;
    }

    /**
     * This Method use for convert Octal to Binary
     * @param octal octal
     * @return binary
     */
    public static long octal2Binary(long octal) {
        int i = 0;
        int decimal = 0;
        int binary = 0;

        // converting octal number
        // into its decimal equivalent
        while (octal != 0) {
            decimal += (octal % 10) * Math.pow(8, i);
            ++i;
            octal /= 10;
        }

        i = 1;

        // converting generated decimal number
        // to its binary equivalent
        while (decimal != 0) {
            binary += (decimal % 2) * i;
            decimal /= 2;
            i *= 10;
        }

        // returning the final result
        return binary;
    }

    /**
     * This Method use for convert Octal to Hexadecimal
     * @param octal octal
     * @return hexadecimal
     */
    public static long octal2Hex(long octal) {
        long rem, decimal = 0;
        int i = 0;
        char[] hexadecimal = new char[20];
        // converting octal to decimal
        while (octal != 0) {
            rem = octal % 10;
            decimal = (int) (decimal + (rem * Math.pow(8, i)));
            i++;
            octal = octal / 10;
        }

        // converting decimal to hexadecimal
        while (decimal != 0) {
            rem = decimal % 16;
            if (rem < 10)
                rem = rem + 48;
            else
                rem = rem + 55;
            hexadecimal[i] = (char) rem;
            i++;
            decimal = decimal / 16;
        }
        return hexadecimal[i];
    }

    /**
     * This Method use for convert Decimal to Binary
     * @param decimal decimal
     * @return Binary
     */
    public static long decimal2Binary(long decimal) {
        // To store the binary number
        long Binary = 0;
        long cnt = 0;
        while (decimal != 0) {
            long rem = decimal % 2;
            double c = Math.pow(10, cnt);
            Binary += rem * c;
            decimal /= 2;

            // Count used to store exponent value
            cnt++;
        }

        return Binary;
    }

    /**
     * This Method use for convert Decimal to Octal
     * @param decimal decimal
     * @return octal
     */
    public static String decimal2Octal(long decimal) {
        long rem; //declaring variable to store remainder
        StringBuilder octal = new StringBuilder(); //declareing variable to store octal
        //declaring array of octal numbers
        char octalchars[] = {'0', '1', '2', '3', '4', '5', '6', '7'};
        //writing logic of decimal to octal conversion
        while (decimal > 0) {
            rem = decimal % 8;
            octal.insert(0, octalchars[(int) rem]);
            decimal = decimal / 8;
        }

        return octal.toString();
    }

    /**
     * This Method use for convert Decimal to Hex
     * @param decimal decimal
     * @return hex
     */
    public static String decimal2Hex(long decimal) {
        long rem;
        StringBuilder hex = new StringBuilder();
        char hexchars[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        while (decimal > 0) {
            rem = decimal % 16;
            hex.insert(0, hexchars[(int) rem]);
            decimal = decimal / 16;
        }
        return hex.toString();
    }

    /**
     * This Method use for convert Hex to Binary
     * @param hex hex
     */
    public static void hex2Binary(char hex[]) {
            int i = 0;

            while (hex[i] != '\u0000') { //null character

                switch (hex[i]) {
                    case '0' -> System.out.print("0000");
                    case '1' -> System.out.print("0001");
                    case '2' -> System.out.print("0010");
                    case '3' -> System.out.print("0011");
                    case '4' -> System.out.print("0100");
                    case '5' -> System.out.print("0101");
                    case '6' -> System.out.print("0110");
                    case '7' -> System.out.print("0111");
                    case '8' -> System.out.print("1000");
                    case '9' -> System.out.print("1001");
                    case 'A', 'a' -> System.out.print("1010");
                    case 'B', 'b' -> System.out.print("1011");
                    case 'C', 'c' -> System.out.print("1100");
                    case 'D', 'd' -> System.out.print("1101");
                    case 'E', 'e' -> System.out.print("1110");
                    case 'F', 'f' -> System.out.print("1111");
                    default -> System.out.print("\nInvalid hexadecimal digit " + hex[i]);
                }
                i++;
            }

        }

    /**
     * This Method use for convert Hex to Octal
     * @param hex hex
     */
        public static void hex2Octal(String hex){
            int dec = 0;

            // taking 1AC as an example of hexadecimal Number.

            int c = hex.length() - 1;

            // finding the decimal equivalent of the
            // hexa decimal number
            for(int i = 0; i < hex.length() ; i ++ )
            {
                //extracting each character from the string.
                char ch = hex.charAt(i);
                switch (ch) {
                    case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' -> {
                        dec = dec + Integer.parseInt(Character.toString(ch)) *
                                (int) Math.pow(16, c);
                        c--;
                    }
                    case 'a', 'A' -> {
                        dec = dec + 10 * (int) Math.pow(16, c);
                        c--;
                    }
                    case 'b', 'B' -> {
                        dec = dec + 11 * (int) Math.pow(16, c);
                        c--;
                    }
                    case 'c', 'C' -> {
                        dec = dec + 12 * (int) Math.pow(16, c);
                        c--;
                    }
                    case 'd', 'D' -> {
                        dec = dec + 13 * (int) Math.pow(16, c);
                        c--;
                    }
                    case 'e', 'E' -> {
                        dec = dec + 14 * (int) Math.pow(16, c);
                        c--;
                    }
                    case 'f', 'F' -> {
                        dec = dec + 15 * (int) Math.pow(16, c);
                        c--;
                    }
                    default -> System.out.println("Invalid hexa input");
                }
            }

            // String oct to store the octal equivalent of a hexadecimal number.
            StringBuilder oct = new StringBuilder();

            //converting decimal to octal number.
            while(dec > 0)
            {
                oct.insert(0, dec % 8);
                dec = dec / 8;
            }
            // Printing the final output.
            System.out.println("Result of Hex to Octal : "+oct);
        }

    /**
     * This Method use for convert Hex to Decimal
     * @param hex hex
     * @return decimal
     */
        public static long hex2Decimal(String hex){
            int len = hex.length();

            // Initializing base value to 1, i.e 16^0
            int base = 1;

            int decimal = 0;

            // Extracting characters as digits from last
            // character
            for (int i = len - 1; i >= 0; i--) {
                // if character lies in '0'-'9', converting
                // it to integral 0-9 by subtracting 48 from
                // ASCII value
                if (hex.charAt(i) >= '0'
                        && hex.charAt(i) <= '9') {
                    decimal += (hex.charAt(i) - 48) * base;

                    // incrementing base by power
                    base = base * 16;
                }

                // if character lies in 'A'-'F' , converting
                // it to integral 10 - 15 by subtracting 55
                // from ASCII value
                else if (hex.charAt(i) >= 'A'
                        && hex.charAt(i) <= 'F') {
                    decimal += (hex.charAt(i) - 55) * base;

                    // incrementing base by power
                    base = base * 16;
                }
            }
            return decimal;
        }
}

